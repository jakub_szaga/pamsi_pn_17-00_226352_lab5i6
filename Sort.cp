#include "stdafx.h"
#include "iostream"
#include <string>
#include <ctime>
#include <stack>
#include <math.h>

using namespace std;

template <typename pizza>
class stos {
private:
	int rozmiar;
	pizza *T;
	pizza *pomoc;
	pizza *Tpocz;
	int koniec;

public:
	stos(int p);
	void dodaj(pizza liczba);
	void usun_wszystko();
	void usun();
	void wyswietl();
	void zeruj();
	void merge(int pocz, int sr, int kon);
	void mergesort(int pocz, int kon);
	int partition2(int p, int r);
	void quicksort2(int p, int r);
	void Insertion_Sort(int N);
	void Heapify(pizza T[], int i, int N);
	void Heap_Sort(pizza T[], int N);
	//void Exchange(int i, int j);
	void Hybrid_Introspective_Sort(int N);
	void IntroSort(int N, int M, double depth);
	void odwroc();
	void nadaj();
	void zmien_pocz();
	void zamien(int p, int l);
	pizza last();
	int size();
};

template<typename pizza>
stos<pizza>::stos(int p)
{
	rozmiar = p;
	T = new pizza[rozmiar];
	pomoc = new pizza[rozmiar];
	koniec = -1;
}

template<typename pizza>
void stos<pizza>::zeruj()
{
	rozmiar = 2;
	T = new pizza[rozmiar];
	pomoc = new pizza[rozmiar];
	koniec = -1;
}

template<typename pizza>
pizza stos<pizza>::last()
{
	return T[rozmiar - 1];
}

template<typename pizza>
int stos<pizza>::size()
{
	return rozmiar;
}

template<typename pizza>
void stos<pizza>::usun() {
	if (koniec == (-1))
	{
		cout << "stos jest pusty" << endl;
	}
	else
	{
		koniec = koniec - 1;
	}
}

template<typename pizza>
void stos<pizza>::usun_wszystko() {
	while (koniec != -1)
	{
		koniec = -1;
	}
}

template<typename pizza>
void stos<pizza>::dodaj(pizza liczba) {
	if (koniec == rozmiar - 1)
	{
		pizza *H = new pizza[rozmiar];
		for (int i = 0; i < rozmiar; i++)
		{
			H[i] = T[i];
		}
		delete T;
		int cos = rozmiar;
		rozmiar = 2 * rozmiar;
		T = new pizza[rozmiar];
		for (int i = 0; i < cos; i++)
		{
			T[i] = H[i];
		}
		delete H;
		delete pomoc;
		pomoc = new pizza[rozmiar];
	}
	koniec = koniec + 1;
	T[koniec] = liczba;
	//Tpocz = new pizza[rozmiar];
	//Tpocz = T;
}

template<typename pizza>
void stos<pizza>::wyswietl() {
	int pomoc;
	if (koniec > -1)
	{
		pomoc = koniec;

		while (pomoc != -1)
		{
			cout << T[pomoc] << endl;
			pomoc = pomoc - 1;
		}
	}
	else
		cout << "Stos jest pusty." << endl;
}

template <typename pizza>
int stos<pizza>::partition2(int p, int r)
{
	int x = T[p];
	int i = p, j = r, w;
	while (true)
	{
		while (T[j] > x)
			j--;
		while (T[i] < x)
			i++;
		if (i < j)
		{
			w = T[i];
			T[i] = T[j];
			T[j] = w;
			i++;
			j--;
		}
		else
			return j;
	}
}

template<typename pizza>
void stos<pizza>::quicksort2(int p, int r)
{
	int q;
	if (p < r)
	{
		q = partition2(p, r);
		quicksort2(p, q);
		quicksort2(q + 1, r); 
	}
}

template<typename pizza>
void stos<pizza>::merge(int pocz, int pivot, int kon)
{
	int i, j, q;
	for (i = pocz; i <= kon; i++)
	{
		pomoc[i] = T[i];
	}
		i = pocz;
		j = pivot + 1;
		q = pocz;
	while (i <= pivot && j <= kon)
	{
		if (pomoc[i]<pomoc[j])
			T[q++] = pomoc[i++];
		else
			T[q++] = pomoc[j++];
	}
	while (i <= pivot) 
		T[q++] = pomoc[i++]; 
}

template<typename pizza>
void stos<pizza>::mergesort(int pocz, int kon)
{
	int pivot;
	if (pocz<kon)
	{
		pivot = (pocz + kon) / 2;
		mergesort(pocz, pivot);
		mergesort(pivot + 1, kon);
		merge(pocz, pivot, kon);
	}
}

template <typename pizza>
void stos<pizza>::Insertion_Sort(int N)
{
	int i, j;
	pizza temp;
	for (i = 1; i<N; ++i)
	{
		temp = T[i];
		for (j = i; j>0 && temp<T[j - 1]; --j)
			T[j] = T[j - 1];
		T[j] = temp;
	}
}

template <typename pizza>
void stos<pizza>::Heapify(pizza T[], int heapSize, int index)
{
	int left = (index + 1) * 2 - 1;
	int right = (index + 1) * 2;
	int largest = left;

	if (left < heapSize && T[left] > T[index])
		largest = left;
	else
		largest = index;

	if (right < heapSize && T[right] > T[largest])
		largest = right;

	if (largest != index)
	{
		pizza temp = T[index];
		T[index] = T[largest];
		T[largest] = temp;

		Heapify(T, heapSize, largest);
	}
}

template <typename pizza>
void stos<pizza>::zamien(int l, int p)
{
	pizza temp = T[p];
	T[p] = T[l];
	T[l] = temp;
}

template <typename pizza>
void stos<pizza>::Heap_Sort(pizza T[] ,int count)
{
	int heapSize = count;

	for (int p = (heapSize) / 2 - 1; p >= 0; p--)
	{
		Heapify(T, heapSize, p);
	}
	
	for (int i = count-1; i >= 0; i--)
	{
		zamien(0, i);

		//heapSize--;
		Heapify(T, i, 0);
	}
}

template <typename pizza>
void stos<pizza>::IntroSort(int left, int right, double depth)
{
	if (depth > 0)
	{
		if (left < right)
		{
			int q = partition2(left, right);
			IntroSort(left, q, depth - 1);
			IntroSort(q + 1, right, depth - 1);
		}
	//	cout << "tut ";
		//mergesort(left, right - 1);
		//Heap_Sort(&T[left], (right-left+1));
	}
	else
	{
		Heap_Sort(&T[left], (right - left + 1));
	}
}

template <typename pizza>
void stos<pizza>::Hybrid_Introspective_Sort(int N)
{
	int depth = (int)floor(1.43*log2(N));
	IntroSort(0, N, depth);
}

template <typename pizza>
void stos<pizza>::odwroc()
{
	pomoc = T;
	T = new pizza[rozmiar];
	for (int i = 0; i <= koniec; i++)
	{
		T[i] = pomoc[koniec - i];
	}
}

template <typename pizza>
void stos<pizza>::nadaj()
{
	for (int i = 0; i <= koniec; i++)
		T[i] = Tpocz[i];
}

template <typename pizza>
void stos<pizza>::zmien_pocz()
{
	Tpocz = new pizza[rozmiar];
	for(int i =0; i<=koniec; i++)
	Tpocz[i] = T[i];
}

int main()
{
	int ilosc = 0;
	clock_t t1, t2, TC = 0;
	stos<int> *T = new stos<int>(5);
	double procent;
	int powtorz = 0;
	cout << "Podaj rozmiar tablic: ";
	cin >> ilosc;
	cout << endl;
	cout << "Ile tablic ma byc przesortowanych: ";
	cin >> powtorz;
	cout << endl;
	cout << "Wypelnianie: " << endl;
	t1 = clock();
	for (int i = 0; i < ilosc; i++)
	{
		T->dodaj(rand() % 10);
		//cout << i << endl;
	}
	//T->wyswietl();
	//T->odwroc(); //ODWRACANIE !!!!! 
	T->zmien_pocz();
	//cout << "cos" << endl;
	t2 = clock();
	//T->zmien_pocz();
	cout << (double)(t2 - t1) / CLOCKS_PER_SEC * 1000 << " ms.\n";
	cout << "Jaki procent tablicy ma byc posortowany przed sortowaniem?  ";
	cin >> procent;
	cout << endl;
	procent = procent / 100;
	T->quicksort2(0, (ilosc-1)*procent);
	//T->wyswietl();
	//T->nadaj();
	//T->wyswietl();
	T->zmien_pocz();
	//T->nadaj();
	//T->wyswietl();

	cout << "Merge: " << endl;
	for (int i = 0; i < powtorz; i++)
	{
		T->nadaj();
		t1 = clock();
		T->mergesort(0, ilosc - 1);
		t2 = clock();
		TC = TC + t2 - t1;
		//T->wyswietl();
	}
	//T->nadaj();
	//T->wyswietl();
	cout << "Po sortowaniu: " << endl;
	//T->wyswietl();
	cout << (double)(TC) / CLOCKS_PER_SEC * 1000 << " ms.\n";
	TC = 0;
	cout << "Quick: " << endl;
	//T->wyswietl();
	t1 = clock();
	for (int i = 0; i < powtorz; i++)
	{
		T->nadaj();
		//T->wyswietl();
		t1 = clock();
		T->quicksort2(0, ilosc - 1);
		t2 = clock();
		TC = TC + t2 - t1;
		//T->wyswietl();
	}
	t2 = clock();
	cout << "Po sortowaniu: " << endl;
	//T->wyswietl();
	cout << (double)(TC) / CLOCKS_PER_SEC * 1000 << " ms.\n";
	TC = 0;
	cout << "Intro: " << endl;
	t1 = clock();
	for (int i = 0; i < powtorz; i++)
	{
		T->nadaj();
		//T->wyswietl();
		t1 = clock();
		T->Hybrid_Introspective_Sort(ilosc - 1);
		t2 = clock();
		TC = TC + t2 - t1;
		//T->wyswietl();
	}
	t2 = clock();
	cout << "Po sortowaniu: " << endl;
	//T->wyswietl();
	cout << (double)(TC) / CLOCKS_PER_SEC * 1000 << " ms.\n";
}
